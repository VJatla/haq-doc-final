# Dataset
<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [Dataset](#dataset)
    - [Standardizing the frame rate](#standardizing-the-frame-rate)
    - [Table region of interest](#table-region-of-interest)
    - [Trimmed videos](#trimmed-videos)
    - [Ground truth](#ground-truth)
        - [Videos in sessions having roi, typing and writing](#videos-in-sessions-having-roi-typing-and-writing)
        - [Typing table-roi trims, manually selected](#typing-table-roi-trims-manually-selected)
            - [Training](#training)
            - [Validation](#validation)

<!-- markdown-toc end -->

## Standardizing the frame rate
AOLME videos are captured at 30 and 60 FPS. For consistency we transcode
everything to 30 FPS. We use FFMPEG for this. The command is,
```sh
# For one file
ffmpeg -threads 0 -i <input video> -filter:v fps=fps=30 <output video>
# To loop over all files in a directory
for i in *.mp4; do ffmpeg -y -i "$i" -filter:v fps=fps=30 "${i%.*}_30fps.mp4"; done
```

## Table region of interest
This is a very fast way of dividing table into regions associated with students.
The protocol and software is outlined in [./table_roi](./table-roi/table-roi.md)

## Trimmed videos

Trimmed videos are used to train classifier. Please follow the link to get more information
[./trimmed_videos](./trimmed_videos/index.md)

## Ground truth
The following table provides video indices for which we have table-roi, typing and writing ground truths.

| **Idx** | **Session**   | **Videos having roi-gt**                     | **Videos having ty-gt**    | **Videos having wr-gt**   |
|---------|---------------|----------------------------------------------|----------------------------|---------------------------|
|         | *C1L1P*       |                                              |                            |                           |
| 01      | C1L1P-02-16-A | {8}                                          | {8}                        | x                         |
| 02      | C1L1P-02-25-A | {14}                                         | {14}                       | x                         |
| 03      | C1L1P-03-02-A | {7}                                          | {7}                        | x                         |
| 04      | C1L1P-03-09-A | {1}                                          | {1}                        | x                         |
| 05      | C1L1P-04-06-A | {5}                                          | {5}                        | x                         |
| 06      | C1L1P-04-13-A | {6}                                          | {6}                        | x                         |
| 07      | C1L1P-04-20-A | {3}                                          | {3}                        | x                         |
|         |               |                                              |                            |                           |
| 08      | C1L1P-03-02-B | {1,2,3,4,5,6,7,8} - ERROR                    | {7,8}                      | {1,2,3,4,5,6,7,8}         |
| 09      | C1L1P-03-09-B | {2}                                          | {2}                        | x                         |
| 10      | C1L1P-03-30-B | {1}                                          | {1}                        | x                         |
| 11      | C1L1P-04-06-B | {2}                                          | {2}                        | x                         |
| 12      | C1L1P-04-27-B | {5,6}                                        | {5,6}                      | x                         |
| 13      | C1L1P-05-04-B | {3}                                          | {3}                        | x                         |
| 14      | C1L1P-05-06-B | {10}                                         | {10}                       | x                         |
| 15      | C1L1P-05-11-B | {3}                                          | {3}                        | x                         |
|         |               |                                              |                            |                           |
| 16      | C1L1P-02-16-C | {3,5,6,7,8}                                  | {8}                        | {3,5,6,7}                 |
| 17      | C1L1P-02-25-C | {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15}  (all) | {1,2,3,4,5,6,7,8,10,14,15} | {5,7,9,10,11,12,13,14,15} |
| 18      | C1L1P-03-02-C | {2}                                          | {2}                        | x                         |
| 19      | C1L1P-03-09-C | {2,3}                                        | {3}                        | {2,3}                     |
| 20      | C1L1P-03-30-C | {1,2,3,4,5,6}                                | {1,2,3,4,5,6}     (all)    | {1,2,3,4,5,6}   (all)     |
| 21      | C1L1P-04-06-C | {1,2,3,4,5}                                  | x                          | {1,2,3,4,5}               |
| 22      | C1L1P-04-13-C | {1,2,3,4,5,6,7}                              | {1,2,3,4,5,6,7}   (all)    | {1,2,3,4,5,6,7}  (all)    |
| 23      | C1L1P-04-20-C | {1,2,3,4,5,6,7}                              | {5,7}                      | {1,2,3,4,5,6,7}   (all)   |
| 24      | C1L1P-05-04-C | {1,2,3,4,5,6,7} - ERROR                      | {1,2,3,4,5,6,7}  (all)     | {1,2}                     |
| 25      | C1L1P-05-11-C | {1}                                          | x                          | {1}                       |
|         |               |                                              |                            |                           |
| 27      | C1L1P-03-09-D | {1,2,4}                                      | {2}                        | {1,4}                     |
| 28      | C1L1P-03-30-D | {1,2,3,4,5,6,7}                              | x                          | {1,2,3,4,5,6,7}   (all)   |
| 29      | C1L1P-04-06-D | {1,2,3}                                      | {2,3}                      | {1,3}                     |
|         |               |                                              |                            |                           |
| 30      | C1L1P-02-25-E | {3}                                          | {3}                        | x                         |
| 31      | C1L1P-03-02-E | {1,2,3,4,5,6,7,8}                            | {1,2,3,4,5,6,7,8}  (all)   | {1,2,3,4,5,6,7,8}   (all) |
|---------|---------------|----------------------------------------------|----------------------------|---------------------------|
|         | *C1L1W*       |                                              |                            |                           |
| 32      | C1L1W-02-14-A | {2,3,4,5,6,7}                                | x                          | {2,3,4,5,6,7}             |
| 33      | C1L1W-02-21-A | {3,5,7,8}                                    | {7}                        | {3,5,8}                   |
| 34      | C1L1W-02-28-A | {1,2,3,4}                                    | {1,2}                      | {3,4}                     |
| 35      | C1L1W-03-07-A | {2}                                          | {2}                        | x                         |
| 36      | C1L1W-03-28-A | {2}                                          | {2}                        | x                         |
| 37      | C1L1W-04-04-A | {2, 5}                                       | x                          | {2,5}                     |
| 38      | C1L1W-04-25-A | {5}                                          | {5}                        | x                         |
|         |               |                                              |                            |                           |
| 39      | C1L1W-05-06-B | {2}                                          | {2}                        | x                         |
|         |               |                                              |                            |                           |
| 40      | C1L1W-02-21-C | {3}                                          | {3}                        | x                         |
|         |               |                                              |                            |                           |
| 41      | C1L1W-02-28-D | {8}                                          | {8}                        | x                         |
|---------|---------------|----------------------------------------------|----------------------------|---------------------------|
|         | *C2L1P*       |                                              |                            |                           |
| 42      | C2L1P-02-23-B | {1,2,3,4,5,6}                                | {1,2,3,4,5,6}     (all)    | {1,2,3}                   |
| 43      | C2L1P-04-12-C | {1,2,3,4,5}                                  | {3,4}                      | {1,2,3,4,5}               |
| 44      | C2L1P-03-08-D | {1,2,3,4,5}                                  | {1,2,3,4,5}        (all)   | {1,2,3,4,5}    (all)      |
| 45      | C2L1P-04-12-E | {1,2,3,4,5}                                  | x                          | {1,2,3,4,5}               |
|---------|---------------|----------------------------------------------|----------------------------|---------------------------|
|         | *C2L1W*       |                                              |                            |                           |
| 46      | C2L1W-02-20-A | {2,3}                                        | x                          | {2,3}                     |
| 47      | C2L1W-04-10-A | {1,2,3}                                      | {1,2}                      | {3}                       |
| 48      | C2L1W-02-27-B | {1,2,3,4}                                    | {1,2,3,4}    (all)         | {2,3,4}                   |
|---------|---------------|----------------------------------------------|----------------------------|---------------------------|
|         | *C3L1P*       |                                              |                            |                           |
| 49      | C3L1P-04-11-C | {1,2,3,4,5}                                  | {1,2,3,4,5}    (all)       | {1,2,3,4,5}     (all)     |
| 50      | C3L1P-02-14-D | {1}                                          | x                          | {1}                       |
| 51      | C3L1P-02-21-D | {1,2,3,4,5}                                  | {1,2,3,4,5}   (all)        | {1,2,3,4}                 |
|---------|---------------|----------------------------------------------|----------------------------|---------------------------|
|         | *C3L1W*       |                                              |                            |                           |
| 52      | C3L1W-03-19-D | {1,2,3,4}                                    | {1,2,3,4}   (all)          | {1,2,3,4}  (all)          |
|---------|---------------|----------------------------------------------|----------------------------|---------------------------|
