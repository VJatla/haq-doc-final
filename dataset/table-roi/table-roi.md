# Table ROI
Here we outline the process of manually annotating table regions. 
The goal is to design a very fast protocol to outline and associate 
table regions to persons.
<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [Table ROI](#table-roi)
    - [Framework overview](#framework-overview)
    - [Details](#details)
        - [0. Protocol / Guiding priciples](#0-protocol--guiding-priciples)
        - [1. Creating a session video](#1-creating-a-session-video)
        - [2. Labeling using Matlab Video Labeler](#2-labeling-using-matlab-video-labeler)
        - [3. `mat` to `csv`](#3-mat-to-csv)
        - [4. per Video annotation](#4-per-video-annotation)

<!-- markdown-toc end -->

## Framework overview
1. **Create session video:** We first combined all the videos in a session
	taking one frame every second. We call this file, `session_video.mp4`.
	A video that has $`t`$ seconds will contribute  $`t+1`$ frames.

2. **Label bounding boxes:** We then used MATLAB 2021b video labeler to label 
	bounding boxes on `session_vide.mp4` following the [protocol](#protocol). 
	The saved MATLAB session is  `session_roi.mat` which is exported to a mat 
	file, `session_roi_exported.mat`.

3. **Export to CSV:** The mat files are hard to read and process in python. So
	we use the script, `./table-roi/mat_to_csv.mat` to convert to CSV file, `
	session_roi.csv`.

4. **Create video level table-roi:** We now have table regions of interest at
	30x speed. From this we create a csv file that maps the regions back to 
	original videos. We use `session_to_vidoes.py` to create `video_roi.csv`

## Details
### 0. Protocol / Guiding priciples
- **Camera view change:** If the view changes, we ignore the
  transition period, and annotate the change with new annotations.
- **When someone leaves:** When someone leaves the scene, and no one else 
  occupies the position, we will keep annotating empty region with the person name.
  The reason is that our keyboard and hand detection algorithms will
  later filter out these instances.
  
### 1. Creating a session video
To create roi we copy necessary videos to a proper directory in
`Dropbox/table_roi_annotation`. We then use the script,
`Dropbox/Marios_Shared/HAQ-AOLME/software/HAQ/table-roi/create_session_videos.py`,
to create a session level video. 

```sh
# from Dropbox/Marios_Shared/HAQ-AOLME/software/HAQ/table-roi/

# To use all the videos in the table-roi directory
python create_session_videos.py /home/vj/Dropbox/table_roi_annotation/C1L1P-A/20170216 all

# To specifially use 8th video from the session
python create_session_videos.py /home/vj/Dropbox/table_roi_annotation/C1L1P-A/20170216 8
```

### 2. Labeling using Matlab Video Labeler
We use MATLAB video labeler to annotate the regions on the table. 
The following [youtube video](https://youtu.be/7pV8YwbFdRw) provides more details. Prof. Pattichis,
Luis Sanchez Tapia and Venkatesh Jatla has access to this video.

[![Image](./pics/YouTube.png)](https://youtu.be/7pV8YwbFdRw)

After labeling please export the session to a `mat` file, `session_roi_exported.mat`.

### 3. `mat` to `csv`
To load the `mat` file easily to python framework we have to convert to a `csv` file.
This can be done using the following script, `mat_to_csv.csv`.

- PATH: `/home/vj/Dropbox/Marios_Shared/HAQ-AOLME/software/HAQ/table-roi/`
- NOTE: Please change `rdir` variable in the script

### 4. per Video annotation
[Step 2](#2-labeling-using-matlab-video-labeler) produces annotaton at session level. These annotations
should be identified at video level to trim activity instances. This can be done by usng the following
script,

- PATH: `/home/vj/Dropbox/Marios_Shared/HAQ-AOLME/software/HAQ/table-roi/`

```sh
python session_roi_to_video.py /home/vj/Dropbox/table_roi_annotation/C1L1P-C/20170420/
```
