# Trimmed videos using table-rois
The following document summarizes trimmed videos extracted using table region of interest (roi).

<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [Trimmed videos using table-rois](#trimmed-videos-using-table-rois)
  - [Extracting trims](#extracting-trims)
  - [Manual selection protocol](#manual-selection-protocol)
    - [typing / no-typing](#typing--no-typing)
    - [writing / no-writing](#writing--no-writing)
  - [Resizing](#resizing)
  - [typing / no-typing](#typing--no-typing-1)
    - [Training samples](#training-samples)
    - [Validation samples](#validation-samples)
    - [Testing samples](#testing-samples)
  - [writing / no-writing](#writing--no-writing-1)
    - [Overview](#overview)
    - [Training samples](#training-samples-1)
    - [Validation samples](#validation-samples-1)
  - [SOTA test runs](#sota-test-runs)

<!-- markdown-toc end -->

## Extracting trims

To extract trims from activity ground truth and table-rois we use `crop_and_trim.py` script.
It is located at `HAQ/activity-labels/gt/trims/crop_and_trim.py` and takes in a JSON file as
input. A sample is displayed below

```json
{
    "videos_dir": "/home/vj/Dropbox/typing-notyping/C1L1P-A/20170406",
    "video_names":[
	"G-C1L1P-Apr06-A-Cesar_q2_05-06_30fps.mp4"
    ],

    "activity": "typing",
    "gt_csv_path": "/home/vj/Dropbox/typing-notyping/C1L1P-A/20170406/gTruth-tynty_30fps.csv",

    "table_roi_dir" : "/home/vj/Dropbox/table_roi_annotation/C1L1P-A/20170406",
    "table_roi_csv" : "video_roi.csv",

    "outdir" : "/home/vj/Dropbox/typing-notyping-trims/C1L1P-A/20170406"
}
```

## Manual selection protocol
After extracting trimmed videos using activity ground truth and tabe-rois,
we manually classify trims into easy and hard cases. The protocol is as following,

### typing / no-typing
- **typing:**
  - Hard: Occluded keyboard or hand
  - Easy: Keyboard and hand are clearly visible.
- **no-typing:**
  - Hard: There is hand on or near the keyboard.
  - Easy: The keyboard does not have any hand around.
  
### writing / no-writing
- **writing:**
  - Hard: ??? Sravani
  - Easy: ??? Sravani
- **no-writing:**
  - Hard: ??? Sravani
  - Easy: ??? Sravani
  
Following the above protocol, in case of typing, `manual-selection` directory is easy and
hard cases sub-directories as show in below. The corresponding samples are copied to them.

```sh
├── manual-selection
│---├── notyping-easy
│---├── notyping-hard
│---├── typing-easy
│---└── typing-hard
```
  
## Resizing
Before training a model I have verified that the size of each trim should be 224 on the
longest edge. To do this I used **ffmpeg** with highest quality profile. For reference the
command is,

```bash
ffmpeg -y -v error -i <input> -vf scale={<out width>}:{<out height>} -preset slow -crf 18 <output location>
```

## typing / no-typing

### Training samples

| Group     | date     |        | no-typing |   |        | typing | Comment                            |
|-----------|----------|--------|-----------|---|--------|--------|------------------------------------|
|           |          | easy   | hard      |   | easy   | hard   |                                    |
| C1L1P-A   | 20170216 | 3      | 0         |   | 5      | 1      |                                    |
|           | 20170225 | 3      | 5         |   | 5      | 1      |                                    |
|           | 20170302 | 0      | 0         |   | 1      | 0      |                                    |
|           | 20170309 | 2      | 0         |   | 0      | 0      |                                    |
|           | 20170406 | 0      | 1         |   | 2      | 0      |                                    |
|           | 20170413 | 2      | 4         |   | 4      | 0      |                                    |
|           | 20170420 | 5      | 4         |   | 6      | 1      |                                    |
| C1L1P-B   | 20170309 | 0      | 3         |   | 1      | 4      |                                    |
|           | 20170330 | 1      | 1         |   | 0      | 0      |                                    |
|           | 20170406 | 0      | 0         |   | 0      | 0      | The no-typing are very bad quality |
|           | 20170427 | 3      | 4         |   | 5      | 3      |                                    |
|           | 20170504 | 0      | 0         |   | 0      | 0      |                                    |
|           | 20170506 | 2      | 3         |   | 6      | 1      |                                    |
|           | 20170511 | 0      | 4         |   | 5      | 0      |                                    |
| C1L1P-C   | 20170216 | 5      | 0         |   | 3      | 4      |                                    |
|           | 20170225 | 8      | 5         |   | 14     | 3      |                                    |
|           | 20170302 | 2      | 3         |   | 7      | 0      |                                    |
|           | 20170309 | 1      | 1         |   | 2      | 0      |                                    |
|           | 20170420 | 0      | 0         |   | 4      | 0      |                                    |
| C1L1P-D   | 20170309 | 0      | 0         |   | 0      | 1      |                                    |
|           | 20170406 | 0      | 0         |   | 5      | 0      |                                    |
| C1L1P-E   | 20170225 | 0      | 0         |   | 2      | 3      |                                    |
| C1L1W-A   | 20170221 | 0      | 0         |   | 2      | 0      |                                    |
|           | 20170228 | 0      | 3         |   | 0      | 8      |                                    |
|           | 20170307 | 0      | 2         |   | 0      | 3      |                                    |
|           | 20170328 | 2      | 5         |   | 0      | 3      |                                    |
|           | 20170425 | 6      | 0         |   | 8      | 1      |                                    |
| C1L1W-B   | 20170506 | 2      | 0         |   | 5      | 3      |                                    |
| C1L1W-C   | 20170221 | 0      | 2         |   | 0      | 3      |                                    |
| C1L1W-D   | 20170228 | 0      | 2         |   | 4      | 0      |                                    |
| C2L1W-A   | 20180410 | 0      | 1         |   | 1      | 2      |                                    |
|           |          |        |           |   |        |        |                                    |
| **Total** |          | **48** | **56**    |   | **98** | **53** |                                    |


### Validation samples

**Manually selected samples**

| Group     | date     | no-typing |        | typing |        | Comments |
| --------- | -------- | --------- | ------ | ------ | ------ | -------- |
|           |          | easy      | hard   | easy   | hard   |          |
| C1L1P-B   | 20170302 | 3         | 0      | 5      | 0      |          |
| C1L1P-C   | 20170504 | 1         | 10     | 4      | 10     |          |
| C2L1P-C   | 20180412 | 3         | 2      | 1      | 0      |          |
| C2L1W-B   | 20180227 | 9         | 12     | 6      | 7      |          |
| C3L1W-D   | 20190319 | 6         | 7      | 14     | 5      |          |
|           |          |           |        |        |        |          |
| **Total** |          | **22**    | **31** | **29** | **22** |          |

**All the table roi samples**


### Testing samples
The testing samples do not go through [Manual selection protocol](#manual-selection-protocol).

| Group     | date     | no-typing |     | typing |     | Comments         |
| --------- | -------- | --------- | --- | ------ | --- | ---------------- |
| C1L1P-C   | 20170413 | 1         |     | 18     |     |                  |
|           | 20170330 | 0         |     | 5      |     | Too less samples |
| C1L1P-E   | 20170302 | 40        |     | 44     |     |                  |
| C2L1P-B   | 20180223 | 30        |     | 48     |     |                  |
| C2L1P-D   | 20180308 | 39        |     | 54     |     |                  |
| C3L1P-C   | 20190411 | 26        |     | 13     |     |                  |
| C3L1P-D   | 20190221 | 42        |     | 31     |     |                  |
|           |          |           |     |        |     |                  |
| **Total** |          |           |     |        |     |                  |





## writing / no-writing

### Overview

### Training samples

### Validation samples


## SOTA test runs
The following test runs are performed to verify SOTA methods that work with table-roi
trimmed videos. Hence, I will be using all the samples to train and test for 4 epochs.
