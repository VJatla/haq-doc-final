# Trimmed videos using object bounding box
The following file describes in detail creation of new trimmed videos to train the following activity classifiers,
1. typing vs no-typing
2. writing vs no-writing.

<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [Trimmed videos](#trimmed-videos)
    - [NOT USED ANYMORE](#not-used-anymore)
    - [Required files](#required-files)
    - [Procedure overview](#procedure-overview)
        - [1. Selecting 90 frames from the middle of activity instance](#1-selecting-90-frames-from-the-middle-of-activity-instance)
        - [2. Getting new bounding box using object coordinates](#2-getting-new-bounding-box-using-object-coordinates)
    - [Software](#software)
        - [Script](#script)
        - [Configuration JSON file](#configuration-json-file)
        - [Output](#output)

<!-- markdown-toc end -->


## NOT USED ANYMORE
** The following procedure is not used. Instead we will be using table region of interests to train the classifiers **

## Required files

## Procedure overview
### 1. Selecting 90 frames from the middle of activity instance
From each activity select 3 seconds (90 frames) from the middle.
```mermaid
graph LR
in[(Typing instances<br/> >=3sec.)] --> 3sec[Selecting 3sec. from <br/>the middle of activity instance]
3sec --> out[(Typing instances<br/> ==3sec.)]
```
  - Let typing instances start at frame $`f0`$ and end at frame $`f0 + f`$.
  - The 3 seconds or 90 frames typing instance will start from $`f0^*`$ and end at frame $`f0^* + 90`$
  ```math
  f0^* = f0 + f/2 - 45
  ```
### 2. Getting new bounding box using object coordinates
Calculate the object detection based activity bounding box using the following objects,
- Writing = hand
- typing  = keyboard

We will only consider bounding boxes that have atleast 50% of their area inside
the activity ground truth. In the image below all the bounding boxes with `x` are
are not used.

<img src="./pics/3sec-object-bounding-box.png" width="600"/>

- Let coordinates of object detections be
  - top left: $`[(x^{tl}_0,y^{tl}_0), (x^{tl}_1,y^{tl}_1), (x^{tl}_2,y^{tl}_2)]`$
  - bottom right: $`[(x^{br}_0,y^{br}_0), (x^{br}_1,y^{br}_1), (x^{br}_2,y^{br}_2)]`$
- Coordinates of bounding box would be
  - top left: $`(\textbf{min}(x^{tl}_0, x^{tl}_1, x^{tl}_2), \textbf{min}(y^{tl}_0, y^{tl}_1, y^{tl}_2))`$
  - bottom right: $`(\textbf{max}(x^{br}_0, x^{br}_1, x^{br}_2), \textbf{max}(y^{br}_0, y^{br}_1, y^{br}_2))`$
  
## Software
### Script
To run the software please use the following python script,

```bash
# from /home/vj/Dropbox/Marios_Shared/HAQ-AOLME/software/HAQ/activity-labels/gt/trims
python crop_and_trim.py cfgs/typing/C1L1P-C/20170413/C1L1P-C_20170413.json
```
### Configuration JSON file
The input to the script is a json file. An example is given below,

```json
{
    "videos_dir": "/home/vj/Dropbox/typing-notyping/C1L1P-C/20170413",
    "video_names":[
	"G-C1L1P-Apr13-C-Windy_q2_01-07_30fps.mp4",
	"G-C1L1P-Apr13-C-Windy_q2_02-07_30fps.mp4",
	"G-C1L1P-Apr13-C-Windy_q2_03-07_30fps.mp4",
	"G-C1L1P-Apr13-C-Windy_q2_04-07_30fps.mp4",
	"G-C1L1P-Apr13-C-Windy_q2_05-07_30fps.mp4",
	"G-C1L1P-Apr13-C-Windy_q2_06-07_30fps.mp4",
	"G-C1L1P-Apr13-C-Windy_q2_07-07_30fps.mp4"
    ],

    "activity": "typing",
    "gt_csv_path": "/home/vj/Dropbox/typing-notyping/C1L1P-C/20170413/gTruth-tynty_30fps.csv",

    "object" : "keyboard",
    "obj_det_dir" : "/home/vj/Dropbox/objectdetection-aolme/keyboard-detectron2/C1L1P-C/20170413",
    "obj_det_csvs" : [
	"G-C1L1P-Apr13-C-Windy_q2_01-07_30fps_60_det_per_min.csv",
	"G-C1L1P-Apr13-C-Windy_q2_02-07_30fps_60_det_per_min.csv",
	"G-C1L1P-Apr13-C-Windy_q2_03-07_30fps_60_det_per_min.csv",
	"G-C1L1P-Apr13-C-Windy_q2_04-07_30fps_60_det_per_min.csv",
	"G-C1L1P-Apr13-C-Windy_q2_05-07_30fps_60_det_per_min.csv",
	"G-C1L1P-Apr13-C-Windy_q2_06-07_30fps_60_det_per_min.csv",
	"G-C1L1P-Apr13-C-Windy_q2_07-07_30fps_60_det_per_min.csv"
    ],

    "table_roi_dir" : "/home/vj/Dropbox/table_roi_annotation/C1L1P-C/20170420/",
    "table_roi_csv" : "video_roi.csv",
    
    "outdir" : "/home/vj/Dropbox/typing-notyping-trims/C1L1P-C/20170413/"
}
```

### Output
The output is organized into following directories. Out of all these directories
we are very interested in `no-writing/with-hand` directory.
```
# Typing
.
├── gt-bbox
│   ├── no-typing
│   │   ├── with-keyboard
│   │   └── without-keyboard
│   └── typing
│       ├── with-keyboard
└── obj-bbox
    ├── no-typing
    │   ├── with-keyboard
    │   └── without-keyboard
    └── typing
        ├── with-keyboard

# Writing
.
├── gt-bbox
│   ├── no-writing
│   │   ├── with-hand
│   │   └── without-hand
│   └── writing
│       ├── with-hand
└── obj-bbox
    ├── no-writing
    │   ├── with-hand
    │   └── without-hand
    └── writing
        ├── with-hand
```
