# Activity classifier
<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [Activity classifier](#activity-classifier)
    - [SOTA files](#sota-files)
    - [typing/no-typing](#typingno-typing)
        - [Manually selected training and validation data](#manually-selected-training-and-validation-data)
        - [Full training and validation data](#full-training-and-validation-data)
        - [](#)
    - [writing/no-writing](#writingno-writing)
        - [Manually selected validation data](#manually-selected-validation-data)
        - [Full validation dataset](#full-validation-dataset)
        - [Augmented training](#augmented-training)

<!-- markdown-toc end -->

## SOTA files
The SOTA files are located in 

## typing/no-typing
### Manually selected training and validation data

**Dataset**
For more details please refer [table-roi-trims.md](../dataset/trimmed_videos/table-roi-trims.md).

| Activity | Training (easy, hard) | Validation (easy, hard) | Testing |
|----------|-----------------------|-------------------------|---------|
| notyping | 104 (48, 56)          | 53 (22, 31)             | 96      |
| typing   | 151 (98, 53)          | 51 (29, 22)             | 133     |

**Results**

| Method   | Best epoch/Total epochs | Trn. Acc. | Val. Acc. | Tst. Acc | Comments                                      |
|----------|-------------------------|-----------|-----------|----------|-----------------------------------------------|
| i3d      | 45/100                  | 97.25     | 88.462    | 75.55    |                                               |
| r2plus1d | 145/180                 | 78.43     | 68.269    | 66.8     |                                               |
| tsm      | 50/50                   | 77.25     | 75.000    | 67.25    | *Since the best == last it needs more epochs* |
| tsn      | 50/100                  | 97.25     | 83.654    | 74.24    |                                               |
| slowfast | 170/208                 | 91.37     | 83.65     | 74.67    |                                               |
|          |                         |           |           |          |                                               |
| Ours     | 15/20                   | 85        | 71        | 70       |                                               |

### Full training and validation data

### 

## writing/no-writing
### Manually selected validation data

**Dataset**

| Activity  | Training (easy, hard) | Validation (easy, hard) | Testing |
|-----------|-----------------------|-------------------------|---------|
| nowriting | 47                    | 34                      |         |
| writing   | 154                   | 69                      |         |

**Results**

| Method | Best epoch/Total epochs | Trn. Acc. | Val. Acc. | Tst. Acc | Comments |
|--------|-------------------------|-----------|-----------|----------|----------|
| Ours   | 11/20                   | 93        | 72        |          |          |
| i3d    | 70/100                  | 94        | 77        |          |          |

### Full validation dataset

### Augmented training
