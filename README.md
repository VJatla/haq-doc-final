---
permalink: index.html
---
# HAQ
<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [HAQ](#haq)
    - [Updates](#updates)
        - [August 2022](#august-2022)

<!-- markdown-toc end -->

## Updates
### August 2022

| Date         | Files                                                                    | Description                                         |
|--------------|--------------------------------------------------------------------------|-----------------------------------------------------|
| Aug 03, 2022 | [table-roi trims](./dataset/trimmed_videos/table-roi-trims.md#test-runs) | Testing SOTA methods that work with table-roi trims |
|              |                                                                          |                                                     |